#include "WalletAsyncContextCounter.h"

namespace CryptoNote {

void WalletAsyncContextCounter::addAsyncContext() {
  std::unique_lock<std::mutex> lock(m_mutex);
  m_asyncContexts++;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletAsyncContextCounter::delAsyncContext() {
  std::unique_lock<std::mutex> lock(m_mutex);
  m_asyncContexts--;

  if (!m_asyncContexts) m_cv.notify_one();
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletAsyncContextCounter::waitAsyncContextsFinish() {
  std::unique_lock<std::mutex> lock(m_mutex);
  while (m_asyncContexts > 0)
    m_cv.wait(lock);
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
} //namespace CryptoNote
