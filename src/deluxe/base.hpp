#pragma once

#include <cstddef>
#include <cstdint>
#include <limits>
#include <initializer_list>
#include <map>

#include "network.hpp"

namespace Evo {

#define CRYPTONOTE_NAME                                                "Evo"
#define GENESIS_COINBASE_TX_HEX                                 "013c01ff00039334023aeb745558b6dace4c59e1cef9d7c820e4320e7da0a56e9bcc0b273b238cb4889334026d51f63bd9ba25dc490332855687d405ae97fb01b3a7c74d0243c87c5be7a8e6e4aa070252d128bc9913d5ee8b702c37609917c2357b2f587e5de5622348a3acd718e5d62101bb6f6487be4d3de09269e9c47778725708a393f52f4601d1325c01e6771c6d5f"

#define GENESIS_NONCE                                                   70
#define GENESIS_TIMESTAMP                                               1529127443

} // CryptoNote
