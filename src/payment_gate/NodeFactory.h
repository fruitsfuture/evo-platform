#pragma once

#include "INode.h"

#include <string>

#include "log/LoggerRef.h"

namespace PaymentService {

class NodeFactory {
	
public:

  static CryptoNote::INode* createNode(const std::string& daemonAddress, uint16_t daemonPort, Logging::ILogger& logger);
  static CryptoNote::INode* createNodeStub();
  
private:

  NodeFactory();
  ~NodeFactory();

  CryptoNote::INode* getNode(const std::string& daemonAddress, uint16_t daemonPort);

  static NodeFactory factory;
};

} //namespace PaymentService
